-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Oct 03, 2020 at 01:43 PM
-- Server version: 5.6.21
-- PHP Version: 7.2.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `router`
--

CREATE TABLE `router` (
  `sapid` int(15) NOT NULL,
  `hostname` varchar(100) DEFAULT NULL,
  `loopback` varchar(100) DEFAULT NULL,
  `macaddress` int(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `router`
--

INSERT INTO `router` (`sapid`, `hostname`, `loopback`, `macaddress`) VALUES
(12, 'cisco.com', '5678', 2334),
(32, 'Rajan', 'Kumar433443', 65),
(43, 'example.com', '76', 8790565),
(344, 'google.com', '6.3.2.1.', 3344);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `router`
--
ALTER TABLE `router`
  ADD PRIMARY KEY (`sapid`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
